<?php

use judahnator\EventLoop\EventLoop;

class FileStreamEventLoop extends EventLoop
{

    private $fileLocation, $fileHandle;
    private $currentLine;
    private $linesRead = 0;

    public function __construct($file)
    {
        $this->fileLocation = $file;
    }

    /**
     * Prepare the application.
     */
    public function loopSetup(): void
    {
        $handle = fopen($this->fileLocation, "r");
        if ($handle) {
            $this->fileHandle = $handle;
        } else {
            throw new RuntimeException("Failed to open file.");
        }

        $this->duringEvent(function(string $currentLine) {
            $currentLineNumber = $this->linesRead + 1;
            echo "#{$currentLineNumber}: {$currentLine}";
        });

        $this->afterEvent(function() {
            $this->linesRead++;
        });
    }

    /**
     * Loop has finished, destruct the application
     */
    public function loopTeardown(): void
    {
        fclose($this->fileHandle);
    }

    /**
     * If this returns true the loop will run once.
     * If this returns false the loop will exit.
     *
     * This acts as the loop rate-limiter as well.
     *
     * @return bool
     */
    public function getRunCondition(): bool
    {
        return ($this->currentLine = fgets($this->fileHandle)) !== false;
    }

    /**
     * The result of this will be passed to the "during event" callbacks.
     *
     * @return mixed
     */
    public function getEventCallbackArgument()
    {
        return $this->currentLine;
    }
}