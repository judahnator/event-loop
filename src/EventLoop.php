<?php

namespace judahnator\EventLoop;


abstract class EventLoop
{

    private $programHalted = false;

    private $beforeEvents = [];
    private $duringEvents = [];
    private $afterEvents = [];
    private $periodicCallbacks = [];

    /**
     * Adds a callback that will run before the event.
     *
     * @param callable $before
     */
    final public function beforeEvent(callable $before): void
    {
        $this->beforeEvents[] = $before;
    }

    /**
     * Adds a callback that will run during the event.
     *
     * The callback will receive a single paramater, and will be the output of
     * the getEventCallbackArgument() function.
     *
     * @param callable $during
     */
    final public function duringEvent(callable $during): void
    {
        $this->duringEvents[] = $during;
    }

    /**
     * Adds a callback that will run after the event.
     *
     * @param callable $after
     */
    final public function afterEvent(callable $after): void
    {
        $this->afterEvents[] = $after;
    }

    /**
     * Adds a callback that will be run periodically, at the set interval in seconds.
     *
     * It is important to note that this will only run on an event, so if you have a
     * periodic callback that is meant to run every 60 seconds but you only have an
     * event once every 5 minutes, your callback will only run once every 5 minutes.
     *
     * @param callable $periodic
     * @param int $seconds
     */
    final public function periodicCallback(callable $periodic, int $seconds = 60): void
    {
        $this->periodicCallbacks[] = [
            'callback' => $periodic,
            'interval' => $seconds,
            'last_run' => time()
        ];
    }

    /**
     * Triggers the halting of the application.
     */
    public function halt(): void
    {
        $this->programHalted = true;
    }

    /**
     * Runs the application.
     *
     * @throws \Exception
     */
    public function run(): void
    {
        // If the program was previously halted, un-halt
        $this->programHalted = false;

        $this->loopSetup();

        while (!$this->programHalted && $this->getRunCondition()) {

            foreach ($this->beforeEvents as $event) {
                if ($event() === false) {
                    continue 2;
                }
            }

            if ($this->programHalted) {
                // User requested program halt, return;
                break;
            }

            $callbackArgument = $this->getEventCallbackArgument();
            foreach ($this->duringEvents as $event) {
                if ($event($callbackArgument) === false) {
                    continue 2;
                }
            }

            foreach ($this->afterEvents as $event) {
                if ($event() === false) {
                    continue 2;
                }
            }

            foreach ($this->periodicCallbacks as $key => $periodicCallback) {
                if ($periodicCallback['interval'] + $periodicCallback['last_run'] <= time()) {
                    $periodicCallback['callback']();
                    $this->periodicCallbacks[$key]['last_run'] = time();
                }
            }

        }

        $this->loopTeardown();

    }

    /**
     * Prepare the application.
     */
    abstract public function loopSetup(): void;

    /**
     * Loop has finished, destruct the application
     */
    abstract public function loopTeardown(): void;

    /**
     * If this returns true the loop will run once.
     * If this returns false the loop will exit.
     *
     * This acts as the loop rate-limiter as well.
     *
     * @return bool
     */
    abstract public function getRunCondition(): bool;

    /**
     * The result of this will be passed to the "during event" callbacks.
     *
     * @return mixed
     */
    abstract public function getEventCallbackArgument();

}