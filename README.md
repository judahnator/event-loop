Event Loop
==========

This might seem initially fairly complicated, but its really not.

So first off, why does this abomination exist? Well I had a use case
when I wanted to use the react framework but you cant really pipe a 
websocket connection into react. So... I built my own event loop system.

I wrote a simple example, check out the `/examples/FileStreamEventLoop.php`
file. It uses the event loop to read a file line-by-line, and prints
each line as it reads it.

Take this example:

```php
<?php

require 'vendor/autoload.php';

$loop = new FileStreamEventLoop('/path/to/your/file.txt');
$loop->run();

/*
#1 first line in your file
#2 second line in your file
#3 third line in your file
...
 */
``` 

To create an event loop you simply need to create a class that extends
the `judahnator\EventLoop\EventLoop` abstract class.

In your `loopSetup()` method you can setup as many before, during, and 
after events for your loop. You can also add a periodic callback
for code you want to be ran periodically.

Everything should be pretty well documented, but if I missed anything
please let me know!